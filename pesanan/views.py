from django.shortcuts import render
from .models import get_pesanan, get_barang_list, delete_pesanan, get_anggota_list, create_pesanan
from django.core.paginator import Paginator
from django.http import HttpResponseRedirect

# Create your views here.
def pesanan(request):
    data = get_pesanan(request)
    paginator = Paginator(data, 10)

    page = request.GET.get('page')
    response = {
        'data':paginator.get_page(page)
    }

    print(response['data'])
    for it in response['data']:
        print(it['nama_item'])
    html = "Toys-VIEW-Pesanan.html"
    # print(response['data'])
    return render(request, html, response)


def pesanan_create(request):
    if (request.method == "POST"):
        create_pesanan(request)
    response = {
        'data_barang':get_barang_list(request),
        'data_anggota':get_anggota_list(request)
    }
    html = "Toys-C-Pesanan.html"    
    return render(request, html, response)


def pesanan_update(request):
    response = {}
    html = "Toys-U-Pesanan.html"
    return render(request, html, response)


def pesanan_search(request, search):
    data = get_pesanan(request)
    result = []
    for item in data:
        if search in item['nama_item']:
            result.append(item)


def pesanan_delete(request, id_pesan):
    delete_pesanan(request, id_pesan)
    return HttpResponseRedirect('/pesanan')