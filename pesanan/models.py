from django.db import models
from django.db import connection

SCHEMA = "db2018010"
PEMESANAN = SCHEMA + ".PEMESANAN"
BARANG = SCHEMA + ".BARANG"
BARANG_PESANAN = SCHEMA + ".BARANG_PESANAN"
PENGGUNA = SCHEMA + ".PENGGUNA"

# Create your models here.
def get_pesanan(self):
    query = """
    SELECT P.id_pemesanan, P.harga_sewa, P.status, B.nama_item
    FROM %s P, %s B, %s BP
    WHERE P.id_pemesanan = BP.id_pemesanan
    AND BP.id_barang = B.id_barang
    """ % (PEMESANAN, BARANG, BARANG_PESANAN)

    # cursor = connection.cursor()
    # cursor.execute(query)
    
    # return cursor

    cursor = connection.cursor()
    cursor.execute(query)
    json = [dict((cursor.description[i][0], value) \
               for i, value in enumerate(row)) for row in cursor.fetchall()]
    cursor.connection.close()
    return parser(json)


def parser(json):
    exist = []
    result = []
    for item in json:
        if item['id_pemesanan'] not in exist:
            item['nama_item'] = [item['nama_item']]
            result.append(item)
            exist.append(item['id_pemesanan'])
        else:
            for data in result:
                if data['id_pemesanan'] == item['id_pemesanan']:
                    data['nama_item'].append(item['nama_item'])
    return result


def get_barang_list(self):
    query = """
    SELECT B.id_barang, B.nama_item
    FROM %s B
    """ % (BARANG)

    cursor = connection.cursor()
    cursor.execute(query)
    json = [dict((cursor.description[i][0], value) \
               for i, value in enumerate(row)) for row in cursor.fetchall()]

    return json


def delete_pesanan(self, id_pesan):
    query = """
    DELETE FROM %s
    WHERE id_pemesanan = '%s';
    """ % (PEMESANAN, id_pesan)
    
    cursor = connection.cursor()
    cursor.execute(query)
    cursor.close()


def get_anggota_list(self):
    query = """
    SELECT P.nama_lengkap, P.no_ktp
    FROM %s P;
    """ % (PENGGUNA)

    cursor = connection.cursor()
    cursor.execute(query)
    json = [dict((cursor.description[i][0], value) \
               for i, value in enumerate(row)) for row in cursor.fetchall()]
    return json


def create_pesanan(self):
    query_id = """
    SELECT id_pemesanan
    FROM %s
    ORDER BY CAST(id_pemesanan AS INT) DESC
    LIMIT 1;
    """ % (PEMESANAN)
    cursor = connection.cursor()
    cursor.execute(query_id)

    last_id = cursor.fetchall()
    print(int(last_id[0][0]) + 1)

