from django.conf.urls import url
from django.urls import path

from . import views

app_name = "pesanan"

urlpatterns = [
    path('', views.pesanan, name='pesanan'),
    path('create', views.pesanan_create, name='pesanan_create'),
    path('update', views.pesanan_update, name='pesanan_update'),
    path('delete/<str:id_pesan>', views.pesanan_delete, name='pesanan_delete'),
]
