from django.conf.urls import url
from django.urls import path

from . import views

app_name = "barang"

urlpatterns = [
    path('', views.barang, name='barang'),  
    path('admin', views.barang_admin, name='barang_admin'),
    path('detail-barang/<str:id>', views.detail_barang, name="info_barang"),
    path('tambah-barang', views.create_barang, name='tambah_barang')
]
