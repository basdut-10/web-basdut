from django.db import models
from django.db import connection

# Create your models here.
barangdb = "db2018010.BARANG"
info_barangdb = "db2018010.INFO_BARANG_LEVEL"
userdb = "db2018010.PENGGUNA"
n = 100
def get_barang(self):
    get_query = """
    SELECT id_barang, nama_item, warna, kondisi
    FROM %s
    """ % (barangdb)
    cursor = connection.cursor()
    cursor.execute(get_query)
    json = [dict((cursor.description[i][0], value) \
               for i, value in enumerate(row)) for row in cursor.fetchall()]
    cursor.connection.close()
    return json

def get_info_barang(self, id):
    get_query = """
    SELECT b.id_barang, b.nama_item, b.warna, b.url_foto, b.kondisi, b.lama_penggunaan
        , p.nama_lengkap, i.nama_level, i.harga_sewa, i.prosi_royalti  
    FROM %s b, %s p, %s i
    WHERE b.id_barang = '%s' AND b.id_barang = i.id_barang AND b.no_ktp_penyewa = p.no_ktp;
    """ % (barangdb, userdb, info_barangdb, id)
    cursor = connection.cursor()
    cursor.execute(get_query)
    json = [dict((cursor.description[i][0], value) \
               for i, value in enumerate(row)) for row in cursor.fetchall()]
    cursor.connection.close()
    return json

def insert_barang(self, id, nama_item, warna, url_foto, kondisi
            , lama_penggunaan, penyewa, b_porsi_royalti, b_harga_sewa, s_porsi_royalti,
            s_harga_sewa, g_porsi_royalti, g_harga_sewa):
    b_porsi_royalti_new = b_porsi_royalti
    s_porsi_royalti_new = s_porsi_royalti
    g_porsi_royalti_new = g_porsi_royalti
    
    if (b_porsi_royalti > 0):
        b_porsi_royalti_new = b_porsi_royalti/100
    if (s_porsi_royalti > 0):
        s_porsi_royalti_new = s_porsi_royalti/100
    if (g_porsi_royalti > 0):
        g_porsi_royalti_new = g_porsi_royalti/100
    query = """
    INSERT INTO %s VALUES (
        '%s', '%s', '%s', '%s', '%s', %d, '%s' 
    ) 
    """ % (barangdb, id, nama_item, warna, url_foto, kondisi, int(lama_penggunaan), penyewa)
    query_bronze = """
    INSERT INTO %s VALUES (
        '%s', '%s', '%f', '%f' 
    ) 
    """ % (info_barangdb, (n+1)+'', b_harga_sewa, b_porsi_royalti_new)
    query_silver = """
    INSERT INTO %s VALUES (
        '%s', '%s', '%f', '%f' 
    ) 
    """ % (info_barangdb, (n+1)+'', s_harga_sewa, s_porsi_royalti_new)
    
    query_gold = """
    INSERT INTO %s VALUES (
        '%s', '%s', '%f', '%f' 
    ) 
    """ % (info_barangdb, (n+1)+'', g_harga_sewa, g_porsi_royalti_new)
    cursor = connection.cursor()
    cursor.execute(query)
    cursor.execute(query_bronze)
    cursor.execute(query_gold)
    cursor.execute(query_silver)
    cursor.connection.close()


