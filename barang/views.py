from django.shortcuts import render
from .models import get_barang, get_info_barang, insert_barang
from django.core.paginator import Paginator

def barang(request):
    data = get_barang(request)
    paginator = Paginator(data, 10)

    page = request.GET.get('page')
    response = {
        'data' : paginator.get_page(page)
    }

    # Debug
    print(response['data'])
    for it in response['data']:
        print(it['nama_item'])   

    html = "list-barang.html"
    return render(request, html, response)

def barang_admin(request):
    data = get_barang(request)
    paginator = Paginator(data, 10)

    page = request.GET.get('page')
    response = {
        'data' : paginator.get_page(page)
    }

    # Debug
    print(response['data'])
    for it in response['data']:
        print(it['nama_item'])   

    html = "list-barang-admin.html"
    return render(request, html, response)

def detail_barang(request, id):
    data = get_info_barang(request, id)
    paginator = Paginator(data, 10)

    page = request.GET.get('page')
    response = {
        'data' : paginator.get_page(page)
    }

    filtered_response = filter_response(response['data'])
    # Debug
    
    print(response['data'])
    print(filtered_response)


    html = "detail-barang.html"
    return render(request, html, filtered_response)

def filter_response(response):
    result = {}
    bronze = {}
    silver = {}
    gold = {}

    for it in response:
        result['id_barang'] = it['id_barang']
        result['nama_item'] = it['nama_item']
        result['warna'] = it['warna']
        result['url_foto'] = it['url_foto']
        result['kondisi'] = it['kondisi']
        result['lama_penggunaan'] = it['lama_penggunaan']
        result['nama_lengkap'] = it['nama_lengkap']
        break
    
    for it in response:
        if it['nama_level'] == "Bronze":
            bronze['porsi_royalti'] = it['prosi_royalti'] * 100
            bronze['harga_sewa'] = it['harga_sewa']        
        elif it['nama_level'] == "Silver":
            silver['porsi_royalti'] = it['prosi_royalti'] * 100
            silver['harga_sewa'] = it['harga_sewa']        
        elif it['nama_level'] == "Gold":
            gold['porsi_royalti'] = it['prosi_royalti'] * 100
            gold['harga_sewa'] = it['harga_sewa']
    result['Bronze'] = bronze 
    result['Silver'] = silver
    result['Gold'] = gold      
    return result

def create_barang(request):
    if (request.method == "POST"):
        id = request.POST['id']
        nama_item = request.POST['nama-item']
        warna = request.POST['warna']
        url_foto = request.POST['url-foto']
        kondisi = request.POST['kondisi']
        lama_penggunaan = request.POST['lama-penggunaan']
        penyewa = request.POST['penyewa']
        b_porsi_royalti = request.POST['b-porsi-royalti']
        b_harga_sewa = request.POST['b-harga-sewa']
        s_porsi_royalti = request.POST['s-porsi-royalti']
        s_harga_sewa = request.POST['s-harga-sewa']
        g_porsi_royalti = request.POST['g-porsi-royalti']
        g_harga_sewa = request.POST['g-harga-sewa']
        
        insert_barang(id, nama_item, warna, url_foto, kondisi
            , lama_penggunaan, penyewa, b_porsi_royalti, b_harga_sewa, s_porsi_royalti,
            s_harga_sewa, g_porsi_royalti, g_harga_sewa)

    response = {}
    html = "insert-form.html"
    return render(request, html, response)