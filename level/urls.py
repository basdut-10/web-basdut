from django.conf.urls import url
from django.urls import path

from . import views

app_name = "level"

urlpatterns = [
    path('', views.level, name='level'),
    path('create', views.level_create, name='level_create'),
    path('update/<str:level_name>', views.level_update, name='level_update'),
    path('delete/<str:level_name>', views.level_delete, name='level_delete'),
]
