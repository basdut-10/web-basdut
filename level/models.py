from django.db import models
from django.db import connection

# Create your models here.

SCHEMA = "db2018010"
LEVEL = SCHEMA + ".LEVEL_KEANGGOTAAN"

# Create your models here.
def get_level(self):
    query = """
    SELECT *
    FROM %s
    ORDER BY minimum_poin;
    """ % (LEVEL)

    # cursor = connection.cursor()
    # cursor.execute(query)
    
    # return cursor

    cursor = connection.cursor()
    cursor.execute(query)
    json = [dict((cursor.description[i][0], value) \
               for i, value in enumerate(row)) for row in cursor.fetchall()]
    cursor.connection.close()

    temp_int = 1
    for temp in json:
        temp['nomor'] = temp_int
        temp['url'] = "/level/update/" + temp['nama_level']
        temp['delete_url'] = "/level/delete/" + temp['nama_level']
        temp_int += 1
    return json 


def get_specific_level(self, level_name):
    query = """
    SELECT *
    FROM %s
    WHERE %s.nama_level = '%s';
    """ % (LEVEL, LEVEL, level_name)

    # cursor = connection.cursor()
    # cursor.execute(query)
    
    # return cursor

    cursor = connection.cursor()
    cursor.execute(query)
    json = [dict((cursor.description[i][0], value) \
               for i, value in enumerate(row)) for row in cursor.fetchall()]
    cursor.connection.close()
    
    return json 


def create_level(self, nama_level, minimum_poin, deskripsi):
    query = """
    INSERT INTO %s
    VALUES ('%s', %d, '%s');
    """ % (LEVEL, nama_level, minimum_poin, deskripsi) 
    cursor = connection.cursor()
    cursor.execute(query)
    cursor.connection.close()
    print(query)


def update_level(self, old_name, nama_level, minimum_poin, deskripsi):
    query = """
    UPDATE %s
    SET nama_level = '%s',
    minimum_poin = %d,
    deskripsi = '%s'
    WHERE nama_level = '%s';
    """ % (LEVEL, nama_level, minimum_poin, deskripsi, old_name)
    cursor = connection.cursor()
    cursor.execute(query)
    cursor.connection.close()


def delete_level(self, level_name):
    query = """
    DELETE FROM %s
    WHERE nama_level = '%s';
    """ % (LEVEL, level_name)
    cursor = connection.cursor()
    cursor.execute(query)
    cursor.connection.close()