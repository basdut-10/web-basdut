from django.shortcuts import render
from .models import get_level, get_specific_level, create_level, update_level, delete_level
from django.http import HttpResponseRedirect
from django.core.paginator import Paginator

# Create your views here.
def level(request):
    data = get_level(request)
    paginator = Paginator(data, 10)

    page = request.GET.get('page')
    response = {
        'data':paginator.get_page(page)
    }

    print(response['data'])
    html = "Toys-VIEW-Level.html"
    # print(response['data'])
    return render(request, html, response)

def level_create(request):
    if (request.method == "POST"):
        nama_level = request.POST['name']
        poin = int(float(request.POST['poin']))
        deskripsi = request.POST['deskripsi']
        try:
            create_level(request, nama_level, poin, deskripsi)
        except:
            pass
    response = {}
    
    html = "Toys-C-Level.html"
    return render(request, html, response)

def level_update(request, level_name):
    if (request.method == "POST"):
        nama_level = request.POST['name']
        poin = int(float(request.POST['poin']))
        deskripsi = request.POST['deskripsi']
        
        update_level(request, level_name, nama_level, poin, deskripsi)
        return HttpResponseRedirect('/level')
    
    response = {
        'data':get_specific_level(request, level_name)
    }
    print(response['data'])
    if response['data']:
        response['data'] = response['data'][0]
        html = "Toys-U-Level.html"
        return render(request, html, response)
    else:
        return HttpResponseRedirect('/level')


def level_delete(request, level_name):
    delete_level(request, level_name)
    return HttpResponseRedirect('/level')