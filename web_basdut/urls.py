"""web_basdut URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import url, include
import profile_basdut.urls as profile_basdut
import pesanan.urls as pesanan
import level.urls as level
import barang.urls as barang
import chat.urls as chat
import review.urls as review
import pengiriman.urls as pengiriman
# import register_basdut.urls as register_basdut
# import subscribe.urls as subscribe


urlpatterns = [
    path('admin/', admin.site.urls),
    path('profile/', include(profile_basdut)),
    path('pesanan/', include(pesanan)),
    path('level/', include(level)),
    path('barang/', include(barang)),
    path('chat/', include(chat)),
    path('review/',include(review)),
    path('pengiriman/',include(pengiriman)),
]
