from django.conf.urls import url
from django.urls import path

from . import views

app_name = "pengiriman"

urlpatterns = [
    path('', views.pengiriman, name='pengiriman'),
]