from django.shortcuts import render
from .models import *
from django.core.paginator import Paginator

# Create your views here.
def chat(request, user = '3671111111111130'):
    chatlist = get_chat(request, user)
    paginator = Paginator(chatlist, 100)
    page = request.GET.get('page')
    response = {
        'chatlist' : paginator.get_page(page)
    }

    print(response['chatlist'])
    for i in response['chatlist']:
        print('==========================')
        print(i['id'])
        print(i['pesan'])
        print(i['date_time'])
        print(i['nama_user'])
        print(i['nama_admin'])
        print('==========================')

    html = "user-chat.html"
    return render(request, html, response)

def admin_chat(request, admin = '3671111111111100', user = '3671111111111130'):
    chatlist = get_chat(request, user)
    paginator = Paginator(chatlist, 100)
    page = request.GET.get('page')
    response = {
        'chatlist' : paginator.get_page(page)
    }

    for i in response['chatlist']:
        response['nama'] = i['nama_user']
        break
    
    print(response['chatlist'])
    for i in response['chatlist']:
        print('==========================')
        print(i['id'])
        print(i['pesan'])
        print(i['date_time'])
        print(i['nama_user'])
        print(i['nama_admin'])
        print('==========================')

    html = "admin-chat.html"
    return render(request, html, response)

def user_who_chat(request, admin = '3671111111111100'):
    userlist = get_user_who_chat(request, admin)
    paginator = Paginator(userlist, 100)
    page = request.GET.get('page')
    response = {
        'userlist' : paginator.get_page(page)
    }

    print(response['userlist'])
    for i in response['userlist']:
        print('==========================')
        print(i['nama_lengkap'])
        print('==========================')

    html = "admin-page-chat.html"
    return render(request, html, response)