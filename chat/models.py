from django.db import models
from django.db import connection

# Create your models here.
userdb = "db2018010.PENGGUNA"
memberdb = "db2018010.ANGGOTA"
admindb = "db2018010.ADMIN"
chatdb = "db2018010.CHAT"
def get_chat(self, user_ktp):
    get_query = """
    SELECT c.id, c.pesan, c.date_time, m.nama_lengkap as nama_user, a.nama_lengkap as nama_admin
    FROM %s c, %s m, %s a
    WHERE m.no_ktp = '%s' AND c.no_ktp_anggota = '%s' AND c.no_ktp_admin = a.no_ktp
    """ % (chatdb, userdb, userdb, user_ktp, user_ktp)
    cursor = connection.cursor()
    cursor.execute(get_query)
    json = [dict((cursor.description[i][0], value) \
               for i, value in enumerate(row)) for row in cursor.fetchall()]
    cursor.connection.close()
    return json

def get_admin_chat(self, user_ktp, admin_ktp):
    get_query = """
    SELECT c.id, c.pesan, c.date_time, m.nama_lengkap as nama_user, a.nama_lengkap as nama_admin
    FROM %s c, %s m, %s a
    WHERE m.no_ktp = '%s' AND a.no_ktp = '%s' AND c.no_ktp_anggota = '%s' AND c.no_ktp_admin = '%s'
        AND c.no_ktp_admin = a.no_ktp AND c.no_ktp_anggota = m.no_ktp
   """ % (chatdb, userdb, userdb, user_ktp, admin_ktp, user_ktp, admin_ktp)
    cursor = connection.cursor()
    cursor.execute(get_query)
    json = [dict((cursor.description[i][0], value) \
               for i, value in enumerate(row)) for row in cursor.fetchall()]
    cursor.connection.close()
    return json

def get_user_who_chat(self, admin_ktp):
    get_query = """
    SELECT m.nama_lengkap, c.no_ktp_anggota
    FROM %s c, %s m
    WHERE c.no_ktp_admin = '%s' AND m.no_ktp = c.no_ktp_anggota 
    """ % (chatdb, userdb, admin_ktp)
    cursor = connection.cursor()
    cursor.execute(get_query)
    json = [dict((cursor.description[i][0], value) \
               for i, value in enumerate(row)) for row in cursor.fetchall()]
    cursor.connection.close()
    return json

