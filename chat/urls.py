from django.conf.urls import url
from django.urls import path

from . import views

app_name = "chat"

urlpatterns = [
    path('', views.chat, name='chat_default'),  
    path('<str:user>', views.chat, name='chat'),  
    path('admin-page/', views.user_who_chat, name='user_default'),
    path('admin-page/<str:admin>', views.user_who_chat, name='user'),
    path('admin-page/chat-to', views.admin_chat, name='admin-chat-default'),
    path('admin-page/<str:admin>/chat-to', views.admin_chat, name='admin-chat-default2'),
    path('admin-page/chat-to/<str:user>', views.admin_chat, name='admin-chat2'),
    path('admin-page/<str:admin>/chat-to/<str:user>', views.admin_chat, name='admin-chat')
]
